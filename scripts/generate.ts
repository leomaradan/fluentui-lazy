import fs from "fs";
import path from "path";

const rootPath = path.resolve(__dirname, "..");
const sourcePath = path.resolve(rootPath, "src");

const exceptions = JSON.parse(
  fs.readFileSync(path.resolve(rootPath, "config", "exceptions.json"), {
    encoding: "utf-8",
  })
);

const componentTemplate = (
  folder: string,
  component: string
) => `export const ${component} = React.lazy(() => import(/* webpackChunkName: "FluentUI-${folder}" */ '@fluentui/react/lib/${folder}').then((module) => ({ default: module.${component} })));
`;

const otherExportsTemplate = (folder: string, exportNames: string[]) =>
  `export { ${exportNames.join(", ")} } from '@fluentui/react/lib/${folder}';`;

const generateFile = async (
  fileName: string
): Promise<string[] | undefined> => {
  const exports = await import(`@fluentui/react/lib/${fileName}`);
  const exportNames = Object.keys(exports).filter((name) => name !== "default");

  const components = exportNames.filter((name) => /^[A-Z][a-z]/.test(name));
  const other = exportNames.filter((name) => !/^[A-Z][a-z]/.test(name));

  if (components.length === 0 && other.length === 0) {
    return;
  }

  const exception = (exceptions[fileName] ?? []) as string[];

  const importReactTemplate = `import React from 'react';
`;
  let content = ``;

  let importReact = false;
  components.forEach((component) => {
    let valid = false;

    const exported = exports[component];
    const type = typeof exported;

    if (type === "object") {
      if (exported["$$typeof"]) {
        valid = true;
      }
    }

    if (type === "function") {
      if (exported.displayName !== undefined) {
        valid = true;
      }

      if (
        exported.prototype &&
        exported.prototype.componentDidMount !== undefined
      ) {
        valid = true;
      }

      if (exported.defaultProps !== undefined) {
        valid = true;
      }
    }

    if (exception.includes(component)) {
      valid = false;
    }

    if (valid) {
      importReact = true;
      content += componentTemplate(fileName, component);
    } else {
      other.push(component);
    }
  });

  if (other.length > 0) {
    content += otherExportsTemplate(fileName, other);
  }

  content += "\n";

  if (importReact) {
    content = importReactTemplate + content;
  }

  const fileExtension = importReact ? ".tsx" : ".ts";
  const filePath = path.resolve(sourcePath, fileName + fileExtension);
  console.log("Writing file", path.relative(sourcePath, filePath));

  fs.writeFileSync(filePath, content);

  return exportNames;
};

const run = async () => {
  fs.rmSync(sourcePath, { recursive: true });
  fs.mkdirSync(sourcePath);

  const files = fs.readdirSync(
    path.resolve(rootPath, "node_modules", "@fluentui", "react", "lib")
  );
  const components = files.filter(
    (file) => file.endsWith(".js") && !file.includes("index")
  );

  const needIndex: { component: string; exports: string[] }[] = [];
  const alreadyImported: string[] = [];

  for await (const componentWithExt of components) {
    const component = componentWithExt.replace(".js", "");
    console.group("Writing", component);
    const response = await generateFile(component);
    if (response) {
      const exports = response.filter(
        (name) => !alreadyImported.includes(name)
      );
      if (exports.length > 0) {
        needIndex.push({
          component,
          exports,
        });
        alreadyImported.push(...exports);
      }
    }
    console.groupEnd();
  }

  const indexContent = needIndex
    .map(
      ({ component, exports }) =>
        `export { ${exports.join(", ")} } from './${component}';`
    )
    .join("\n");
  fs.writeFileSync(path.resolve(sourcePath, "index.ts"), indexContent);

  console.log("Done!");
};

run();
