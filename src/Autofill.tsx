import React from 'react';
export const Autofill = React.lazy(() => import(/* webpackChunkName: "FluentUI-Autofill" */ '@fluentui/react/lib/Autofill').then((module) => ({ default: module.Autofill })));

