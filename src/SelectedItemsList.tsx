import React from 'react';
export const BaseSelectedItemsList = React.lazy(() => import(/* webpackChunkName: "FluentUI-SelectedItemsList" */ '@fluentui/react/lib/SelectedItemsList').then((module) => ({ default: module.BaseSelectedItemsList })));
export const BasePeopleSelectedItemsList = React.lazy(() => import(/* webpackChunkName: "FluentUI-SelectedItemsList" */ '@fluentui/react/lib/SelectedItemsList').then((module) => ({ default: module.BasePeopleSelectedItemsList })));
export const SelectedPeopleList = React.lazy(() => import(/* webpackChunkName: "FluentUI-SelectedItemsList" */ '@fluentui/react/lib/SelectedItemsList').then((module) => ({ default: module.SelectedPeopleList })));
export { ExtendedSelectedItem } from '@fluentui/react/lib/SelectedItemsList';
