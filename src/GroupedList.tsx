import React from 'react';
export const GroupedList = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedList" */ '@fluentui/react/lib/GroupedList').then((module) => ({ default: module.GroupedList })));
export const GroupHeader = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedList" */ '@fluentui/react/lib/GroupedList').then((module) => ({ default: module.GroupHeader })));
export const GroupFooter = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedList" */ '@fluentui/react/lib/GroupedList').then((module) => ({ default: module.GroupFooter })));
export const GroupShowAll = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedList" */ '@fluentui/react/lib/GroupedList').then((module) => ({ default: module.GroupShowAll })));
export const GroupedListSection = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedList" */ '@fluentui/react/lib/GroupedList').then((module) => ({ default: module.GroupedListSection })));
export const GroupedListV2_unstable = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedList" */ '@fluentui/react/lib/GroupedList').then((module) => ({ default: module.GroupedListV2_unstable })));
export { GroupSpacer, GroupedListBase, CollapseAllVisibility, GroupedListV2FC, GroupedListV2Wrapper, GetGroupCount } from '@fluentui/react/lib/GroupedList';
