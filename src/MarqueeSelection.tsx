import React from 'react';
export const MarqueeSelection = React.lazy(() => import(/* webpackChunkName: "FluentUI-MarqueeSelection" */ '@fluentui/react/lib/MarqueeSelection').then((module) => ({ default: module.MarqueeSelection })));
export { SELECTION_CHANGE, SelectionDirection, SelectionMode, Selection, SelectionZone } from '@fluentui/react/lib/MarqueeSelection';
