import React from 'react';
export const Calendar = React.lazy(() => import(/* webpackChunkName: "FluentUI-Calendar" */ '@fluentui/react/lib/Calendar').then((module) => ({ default: module.Calendar })));
export { defaultCalendarStrings, defaultDayPickerStrings, defaultCalendarNavigationIcons, DayOfWeek, DateRangeType, FirstWeekOfYear, AnimationDirection } from '@fluentui/react/lib/Calendar';
