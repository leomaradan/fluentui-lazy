import React from 'react';
export const Checkbox = React.lazy(() => import(/* webpackChunkName: "FluentUI-Checkbox" */ '@fluentui/react/lib/Checkbox').then((module) => ({ default: module.Checkbox })));
export const CheckboxBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Checkbox" */ '@fluentui/react/lib/Checkbox').then((module) => ({ default: module.CheckboxBase })));

