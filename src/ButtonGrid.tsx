import React from 'react';
export const ButtonGrid = React.lazy(() => import(/* webpackChunkName: "FluentUI-ButtonGrid" */ '@fluentui/react/lib/ButtonGrid').then((module) => ({ default: module.ButtonGrid })));
export { ButtonGridCell } from '@fluentui/react/lib/ButtonGrid';
