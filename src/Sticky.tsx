import React from 'react';
export const Sticky = React.lazy(() => import(/* webpackChunkName: "FluentUI-Sticky" */ '@fluentui/react/lib/Sticky').then((module) => ({ default: module.Sticky })));
export { StickyPositionType } from '@fluentui/react/lib/Sticky';
