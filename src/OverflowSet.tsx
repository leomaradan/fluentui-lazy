import React from 'react';
export const OverflowSet = React.lazy(() => import(/* webpackChunkName: "FluentUI-OverflowSet" */ '@fluentui/react/lib/OverflowSet').then((module) => ({ default: module.OverflowSet })));
export const OverflowSetBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-OverflowSet" */ '@fluentui/react/lib/OverflowSet').then((module) => ({ default: module.OverflowSetBase })));

