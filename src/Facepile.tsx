import React from 'react';
export const FacepileBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Facepile" */ '@fluentui/react/lib/Facepile').then((module) => ({ default: module.FacepileBase })));
export const Facepile = React.lazy(() => import(/* webpackChunkName: "FluentUI-Facepile" */ '@fluentui/react/lib/Facepile').then((module) => ({ default: module.Facepile })));
export { OverflowButtonType } from '@fluentui/react/lib/Facepile';
