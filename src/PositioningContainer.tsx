import React from 'react';
export const PositioningContainer = React.lazy(() => import(/* webpackChunkName: "FluentUI-PositioningContainer" */ '@fluentui/react/lib/PositioningContainer').then((module) => ({ default: module.PositioningContainer })));
export { useHeightOffset } from '@fluentui/react/lib/PositioningContainer';
