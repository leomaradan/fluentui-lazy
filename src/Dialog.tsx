import React from 'react';
export const Dialog = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dialog" */ '@fluentui/react/lib/Dialog').then((module) => ({ default: module.Dialog })));
export const DialogBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dialog" */ '@fluentui/react/lib/Dialog').then((module) => ({ default: module.DialogBase })));
export const DialogContent = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dialog" */ '@fluentui/react/lib/Dialog').then((module) => ({ default: module.DialogContent })));
export const DialogContentBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dialog" */ '@fluentui/react/lib/Dialog').then((module) => ({ default: module.DialogContentBase })));
export const DialogFooter = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dialog" */ '@fluentui/react/lib/Dialog').then((module) => ({ default: module.DialogFooter })));
export { DialogFooterBase, DialogType } from '@fluentui/react/lib/Dialog';
