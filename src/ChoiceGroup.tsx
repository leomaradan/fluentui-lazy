import React from 'react';
export const ChoiceGroup = React.lazy(() => import(/* webpackChunkName: "FluentUI-ChoiceGroup" */ '@fluentui/react/lib/ChoiceGroup').then((module) => ({ default: module.ChoiceGroup })));
export const ChoiceGroupBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-ChoiceGroup" */ '@fluentui/react/lib/ChoiceGroup').then((module) => ({ default: module.ChoiceGroupBase })));
export const ChoiceGroupOption = React.lazy(() => import(/* webpackChunkName: "FluentUI-ChoiceGroup" */ '@fluentui/react/lib/ChoiceGroup').then((module) => ({ default: module.ChoiceGroupOption })));

