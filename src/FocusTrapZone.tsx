import React from 'react';
export const FocusTrapZone = React.lazy(() => import(/* webpackChunkName: "FluentUI-FocusTrapZone" */ '@fluentui/react/lib/FocusTrapZone').then((module) => ({ default: module.FocusTrapZone })));

