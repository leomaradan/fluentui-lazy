import React from 'react';
export const Fabric = React.lazy(() => import(/* webpackChunkName: "FluentUI-Fabric" */ '@fluentui/react/lib/Fabric').then((module) => ({ default: module.Fabric })));
export const FabricBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Fabric" */ '@fluentui/react/lib/Fabric').then((module) => ({ default: module.FabricBase })));

