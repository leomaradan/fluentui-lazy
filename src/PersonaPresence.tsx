import React from 'react';
export const PersonaPresence = React.lazy(() => import(/* webpackChunkName: "FluentUI-PersonaPresence" */ '@fluentui/react/lib/PersonaPresence').then((module) => ({ default: module.PersonaPresence })));
export const PersonaPresenceBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-PersonaPresence" */ '@fluentui/react/lib/PersonaPresence').then((module) => ({ default: module.PersonaPresenceBase })));

