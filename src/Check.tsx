import React from 'react';
export const Check = React.lazy(() => import(/* webpackChunkName: "FluentUI-Check" */ '@fluentui/react/lib/Check').then((module) => ({ default: module.Check })));
export const CheckBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Check" */ '@fluentui/react/lib/Check').then((module) => ({ default: module.CheckBase })));

