import React from 'react';
export const KeytipLayer = React.lazy(() => import(/* webpackChunkName: "FluentUI-KeytipLayer" */ '@fluentui/react/lib/KeytipLayer').then((module) => ({ default: module.KeytipLayer })));
export const KeytipLayerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-KeytipLayer" */ '@fluentui/react/lib/KeytipLayer').then((module) => ({ default: module.KeytipLayerBase })));

