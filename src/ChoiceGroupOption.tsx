import React from 'react';
export const ChoiceGroupOption = React.lazy(() => import(/* webpackChunkName: "FluentUI-ChoiceGroupOption" */ '@fluentui/react/lib/ChoiceGroupOption').then((module) => ({ default: module.ChoiceGroupOption })));

