import React from 'react';
export const Tooltip = React.lazy(() => import(/* webpackChunkName: "FluentUI-Tooltip" */ '@fluentui/react/lib/Tooltip').then((module) => ({ default: module.Tooltip })));
export const TooltipBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Tooltip" */ '@fluentui/react/lib/Tooltip').then((module) => ({ default: module.TooltipBase })));
export const TooltipHost = React.lazy(() => import(/* webpackChunkName: "FluentUI-Tooltip" */ '@fluentui/react/lib/Tooltip').then((module) => ({ default: module.TooltipHost })));
export const TooltipHostBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Tooltip" */ '@fluentui/react/lib/Tooltip').then((module) => ({ default: module.TooltipHostBase })));
export { TooltipDelay, TooltipOverflowMode } from '@fluentui/react/lib/Tooltip';
