import React from 'react';
export const Dropdown = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dropdown" */ '@fluentui/react/lib/Dropdown').then((module) => ({ default: module.Dropdown })));
export const DropdownBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Dropdown" */ '@fluentui/react/lib/Dropdown').then((module) => ({ default: module.DropdownBase })));
export { DropdownMenuItemType } from '@fluentui/react/lib/Dropdown';
