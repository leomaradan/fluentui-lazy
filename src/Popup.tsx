import React from 'react';
export const Popup = React.lazy(() => import(/* webpackChunkName: "FluentUI-Popup" */ '@fluentui/react/lib/Popup').then((module) => ({ default: module.Popup })));

