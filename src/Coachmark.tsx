import React from 'react';
export const Coachmark = React.lazy(() => import(/* webpackChunkName: "FluentUI-Coachmark" */ '@fluentui/react/lib/Coachmark').then((module) => ({ default: module.Coachmark })));
export const CoachmarkBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Coachmark" */ '@fluentui/react/lib/Coachmark').then((module) => ({ default: module.CoachmarkBase })));
export { COACHMARK_ATTRIBUTE_NAME } from '@fluentui/react/lib/Coachmark';
