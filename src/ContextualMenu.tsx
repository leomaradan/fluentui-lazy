import React from 'react';
export const ContextualMenu = React.lazy(() => import(/* webpackChunkName: "FluentUI-ContextualMenu" */ '@fluentui/react/lib/ContextualMenu').then((module) => ({ default: module.ContextualMenu })));
export const ContextualMenuBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-ContextualMenu" */ '@fluentui/react/lib/ContextualMenu').then((module) => ({ default: module.ContextualMenuBase })));
export const ContextualMenuItem = React.lazy(() => import(/* webpackChunkName: "FluentUI-ContextualMenu" */ '@fluentui/react/lib/ContextualMenu').then((module) => ({ default: module.ContextualMenuItem })));
export { getMenuItemStyles, getContextualMenuItemClassNames, getContextualMenuItemStyles, getSubmenuItems, canAnyMenuItemsCheck, DirectionalHint, ContextualMenuItemType, ContextualMenuItemBase } from '@fluentui/react/lib/ContextualMenu';
