import React from 'react';
export const SpinButton = React.lazy(() => import(/* webpackChunkName: "FluentUI-SpinButton" */ '@fluentui/react/lib/SpinButton').then((module) => ({ default: module.SpinButton })));
export { KeyboardSpinDirection } from '@fluentui/react/lib/SpinButton';
