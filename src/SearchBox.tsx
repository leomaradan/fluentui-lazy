import React from 'react';
export const SearchBox = React.lazy(() => import(/* webpackChunkName: "FluentUI-SearchBox" */ '@fluentui/react/lib/SearchBox').then((module) => ({ default: module.SearchBox })));
export const SearchBoxBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-SearchBox" */ '@fluentui/react/lib/SearchBox').then((module) => ({ default: module.SearchBoxBase })));

