import React from 'react';
export const ShimmeredDetailsList = React.lazy(() => import(/* webpackChunkName: "FluentUI-ShimmeredDetailsList" */ '@fluentui/react/lib/ShimmeredDetailsList').then((module) => ({ default: module.ShimmeredDetailsList })));
export { getShimmeredDetailsListStyles, ShimmeredDetailsListBase } from '@fluentui/react/lib/ShimmeredDetailsList';
