import React from 'react';
export const Pivot = React.lazy(() => import(/* webpackChunkName: "FluentUI-Pivot" */ '@fluentui/react/lib/Pivot').then((module) => ({ default: module.Pivot })));
export const PivotBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Pivot" */ '@fluentui/react/lib/Pivot').then((module) => ({ default: module.PivotBase })));
export { PivotItem, PivotLinkFormat, PivotLinkSize } from '@fluentui/react/lib/Pivot';
