import React from 'react';
export const Label = React.lazy(() => import(/* webpackChunkName: "FluentUI-Label" */ '@fluentui/react/lib/Label').then((module) => ({ default: module.Label })));
export { LabelBase } from '@fluentui/react/lib/Label';
