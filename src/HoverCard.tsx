import React from 'react';
export const HoverCard = React.lazy(() => import(/* webpackChunkName: "FluentUI-HoverCard" */ '@fluentui/react/lib/HoverCard').then((module) => ({ default: module.HoverCard })));
export const HoverCardBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-HoverCard" */ '@fluentui/react/lib/HoverCard').then((module) => ({ default: module.HoverCardBase })));
export const ExpandingCard = React.lazy(() => import(/* webpackChunkName: "FluentUI-HoverCard" */ '@fluentui/react/lib/HoverCard').then((module) => ({ default: module.ExpandingCard })));
export const ExpandingCardBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-HoverCard" */ '@fluentui/react/lib/HoverCard').then((module) => ({ default: module.ExpandingCardBase })));
export const PlainCard = React.lazy(() => import(/* webpackChunkName: "FluentUI-HoverCard" */ '@fluentui/react/lib/HoverCard').then((module) => ({ default: module.PlainCard })));
export { OpenCardMode, HoverCardType, ExpandingCardMode, PlainCardBase } from '@fluentui/react/lib/HoverCard';
