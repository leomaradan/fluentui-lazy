import React from 'react';
export const Callout = React.lazy(() => import(/* webpackChunkName: "FluentUI-Callout" */ '@fluentui/react/lib/Callout').then((module) => ({ default: module.Callout })));
export const CalloutContent = React.lazy(() => import(/* webpackChunkName: "FluentUI-Callout" */ '@fluentui/react/lib/Callout').then((module) => ({ default: module.CalloutContent })));
export const CalloutContentBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Callout" */ '@fluentui/react/lib/Callout').then((module) => ({ default: module.CalloutContentBase })));
export { FocusTrapCallout, DirectionalHint } from '@fluentui/react/lib/Callout';
