import React from 'react';
export const Slider = React.lazy(() => import(/* webpackChunkName: "FluentUI-Slider" */ '@fluentui/react/lib/Slider').then((module) => ({ default: module.Slider })));
export const SliderBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Slider" */ '@fluentui/react/lib/Slider').then((module) => ({ default: module.SliderBase })));

