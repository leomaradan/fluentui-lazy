import React from 'react';
export const List = React.lazy(() => import(/* webpackChunkName: "FluentUI-List" */ '@fluentui/react/lib/List').then((module) => ({ default: module.List })));
export { ScrollToMode } from '@fluentui/react/lib/List';
