import React from 'react';
export const DatePicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-DatePicker" */ '@fluentui/react/lib/DatePicker').then((module) => ({ default: module.DatePicker })));
export const DatePickerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-DatePicker" */ '@fluentui/react/lib/DatePicker').then((module) => ({ default: module.DatePickerBase })));
export { defaultDatePickerStrings, AnimationDirection } from '@fluentui/react/lib/DatePicker';
