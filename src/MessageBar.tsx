import React from 'react';
export const MessageBar = React.lazy(() => import(/* webpackChunkName: "FluentUI-MessageBar" */ '@fluentui/react/lib/MessageBar').then((module) => ({ default: module.MessageBar })));
export const MessageBarBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-MessageBar" */ '@fluentui/react/lib/MessageBar').then((module) => ({ default: module.MessageBarBase })));
export { MessageBarType } from '@fluentui/react/lib/MessageBar';
