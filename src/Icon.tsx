import React from 'react';
export const Icon = React.lazy(() => import(/* webpackChunkName: "FluentUI-Icon" */ '@fluentui/react/lib/Icon').then((module) => ({ default: module.Icon })));
export { getIconContent, getFontIcon, IconBase, IconType, FontIcon, ImageIcon } from '@fluentui/react/lib/Icon';
