import React from 'react';
export const Overlay = React.lazy(() => import(/* webpackChunkName: "FluentUI-Overlay" */ '@fluentui/react/lib/Overlay').then((module) => ({ default: module.Overlay })));
export const OverlayBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Overlay" */ '@fluentui/react/lib/Overlay').then((module) => ({ default: module.OverlayBase })));

