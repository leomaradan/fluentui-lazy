import React from 'react';
export const CommandBar = React.lazy(() => import(/* webpackChunkName: "FluentUI-CommandBar" */ '@fluentui/react/lib/CommandBar').then((module) => ({ default: module.CommandBar })));
export const CommandBarBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-CommandBar" */ '@fluentui/react/lib/CommandBar').then((module) => ({ default: module.CommandBarBase })));
export { getCommandBarStyles, getCommandButtonStyles } from '@fluentui/react/lib/CommandBar';
