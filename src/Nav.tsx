import React from 'react';
export const Nav = React.lazy(() => import(/* webpackChunkName: "FluentUI-Nav" */ '@fluentui/react/lib/Nav').then((module) => ({ default: module.Nav })));
export const NavBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Nav" */ '@fluentui/react/lib/Nav').then((module) => ({ default: module.NavBase })));
export { isRelativeUrl } from '@fluentui/react/lib/Nav';
