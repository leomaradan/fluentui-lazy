import React from 'react';
export const Persona = React.lazy(() => import(/* webpackChunkName: "FluentUI-Persona" */ '@fluentui/react/lib/Persona').then((module) => ({ default: module.Persona })));
export const PersonaBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Persona" */ '@fluentui/react/lib/Persona').then((module) => ({ default: module.PersonaBase })));
export const PersonaCoin = React.lazy(() => import(/* webpackChunkName: "FluentUI-Persona" */ '@fluentui/react/lib/Persona').then((module) => ({ default: module.PersonaCoin })));
export const PersonaCoinBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Persona" */ '@fluentui/react/lib/Persona').then((module) => ({ default: module.PersonaCoinBase })));
export { getPersonaInitialsColor, personaSize, personaPresenceSize, sizeBoolean, sizeToPixels, presenceBoolean, PersonaSize, PersonaPresence, PersonaInitialsColor } from '@fluentui/react/lib/Persona';
