import React from 'react';
export const BaseFloatingPicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-FloatingPicker" */ '@fluentui/react/lib/FloatingPicker').then((module) => ({ default: module.BaseFloatingPicker })));
export const BaseFloatingPeoplePicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-FloatingPicker" */ '@fluentui/react/lib/FloatingPicker').then((module) => ({ default: module.BaseFloatingPeoplePicker })));
export const FloatingPeoplePicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-FloatingPicker" */ '@fluentui/react/lib/FloatingPicker').then((module) => ({ default: module.FloatingPeoplePicker })));
export const SuggestionsControl = React.lazy(() => import(/* webpackChunkName: "FluentUI-FloatingPicker" */ '@fluentui/react/lib/FloatingPicker').then((module) => ({ default: module.SuggestionsControl })));
export { createItem, SuggestionsStore, SuggestionItemType, SuggestionsHeaderFooterItem, SuggestionsCore } from '@fluentui/react/lib/FloatingPicker';
