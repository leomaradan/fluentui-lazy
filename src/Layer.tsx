import React from 'react';
export const Layer = React.lazy(() => import(/* webpackChunkName: "FluentUI-Layer" */ '@fluentui/react/lib/Layer').then((module) => ({ default: module.Layer })));
export const LayerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Layer" */ '@fluentui/react/lib/Layer').then((module) => ({ default: module.LayerBase })));
export { createDefaultLayerHost, cleanupDefaultLayerHost, getLayerHostSelector, getLayerCount, getLayerHost, notifyHostChanged, registerLayer, registerLayerHost, setLayerHostSelector, unregisterLayer, unregisterLayerHost, getLayerStyles, LayerHost } from '@fluentui/react/lib/Layer';
