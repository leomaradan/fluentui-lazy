import React from 'react';
export const FocusZone = React.lazy(() => import(/* webpackChunkName: "FluentUI-FocusZone" */ '@fluentui/react/lib/FocusZone').then((module) => ({ default: module.FocusZone })));
export { FocusZoneDirection, FocusZoneTabbableElements } from '@fluentui/react/lib/FocusZone';
