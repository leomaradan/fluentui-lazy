export { getBoundsFromTargetWindow, getMaxHeight, getOppositeEdge, positionCallout, positionCard, positionElement, RectangleEdge, Position } from '@fluentui/react/lib/Positioning';
