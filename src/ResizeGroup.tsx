import React from 'react';
export const ResizeGroup = React.lazy(() => import(/* webpackChunkName: "FluentUI-ResizeGroup" */ '@fluentui/react/lib/ResizeGroup').then((module) => ({ default: module.ResizeGroup })));
export const ResizeGroupBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-ResizeGroup" */ '@fluentui/react/lib/ResizeGroup').then((module) => ({ default: module.ResizeGroupBase })));
export { getMeasurementCache, getNextResizeGroupStateProvider, MeasuredContext, ResizeGroupDirection } from '@fluentui/react/lib/ResizeGroup';
