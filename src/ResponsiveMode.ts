export { useResponsiveMode, setResponsiveMode, initializeResponsiveMode, getInitialResponsiveMode, withResponsiveMode, getResponsiveMode, ResponsiveMode } from '@fluentui/react/lib/ResponsiveMode';
