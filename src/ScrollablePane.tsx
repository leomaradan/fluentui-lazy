import React from 'react';
export const ScrollablePane = React.lazy(() => import(/* webpackChunkName: "FluentUI-ScrollablePane" */ '@fluentui/react/lib/ScrollablePane').then((module) => ({ default: module.ScrollablePane })));
export const ScrollablePaneBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-ScrollablePane" */ '@fluentui/react/lib/ScrollablePane').then((module) => ({ default: module.ScrollablePaneBase })));
export { ScrollbarVisibility, ScrollablePaneContext } from '@fluentui/react/lib/ScrollablePane';
