import React from 'react';
export const Shimmer = React.lazy(() => import(/* webpackChunkName: "FluentUI-Shimmer" */ '@fluentui/react/lib/Shimmer').then((module) => ({ default: module.Shimmer })));
export const ShimmerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Shimmer" */ '@fluentui/react/lib/Shimmer').then((module) => ({ default: module.ShimmerBase })));
export const ShimmerLine = React.lazy(() => import(/* webpackChunkName: "FluentUI-Shimmer" */ '@fluentui/react/lib/Shimmer').then((module) => ({ default: module.ShimmerLine })));
export const ShimmerCircle = React.lazy(() => import(/* webpackChunkName: "FluentUI-Shimmer" */ '@fluentui/react/lib/Shimmer').then((module) => ({ default: module.ShimmerCircle })));
export const ShimmerGap = React.lazy(() => import(/* webpackChunkName: "FluentUI-Shimmer" */ '@fluentui/react/lib/Shimmer').then((module) => ({ default: module.ShimmerGap })));
export const ShimmerElementsGroup = React.lazy(() => import(/* webpackChunkName: "FluentUI-Shimmer" */ '@fluentui/react/lib/Shimmer').then((module) => ({ default: module.ShimmerElementsGroup })));
export { ShimmerElementType, ShimmerElementsDefaultHeights, ShimmerLineBase, ShimmerCircleBase, ShimmerGapBase, ShimmerElementsGroupBase } from '@fluentui/react/lib/Shimmer';
