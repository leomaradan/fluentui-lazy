import React from 'react';
export const Text = React.lazy(() => import(/* webpackChunkName: "FluentUI-Text" */ '@fluentui/react/lib/Text').then((module) => ({ default: module.Text })));
export { TextView, TextStyles } from '@fluentui/react/lib/Text';
