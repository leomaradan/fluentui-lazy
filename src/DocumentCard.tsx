import React from 'react';
export const DocumentCard = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCard })));
export const DocumentCardActions = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardActions })));
export const DocumentCardActivity = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardActivity })));
export const DocumentCardDetails = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardDetails })));
export const DocumentCardLocation = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardLocation })));
export const DocumentCardPreview = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardPreview })));
export const DocumentCardImage = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardImage })));
export const DocumentCardTitle = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardTitle })));
export const DocumentCardLogo = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardLogo })));
export const DocumentCardStatus = React.lazy(() => import(/* webpackChunkName: "FluentUI-DocumentCard" */ '@fluentui/react/lib/DocumentCard').then((module) => ({ default: module.DocumentCardStatus })));
export { DocumentCardType } from '@fluentui/react/lib/DocumentCard';
