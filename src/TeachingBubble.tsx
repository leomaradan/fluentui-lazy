import React from 'react';
export const TeachingBubble = React.lazy(() => import(/* webpackChunkName: "FluentUI-TeachingBubble" */ '@fluentui/react/lib/TeachingBubble').then((module) => ({ default: module.TeachingBubble })));
export const TeachingBubbleBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-TeachingBubble" */ '@fluentui/react/lib/TeachingBubble').then((module) => ({ default: module.TeachingBubbleBase })));
export const TeachingBubbleContent = React.lazy(() => import(/* webpackChunkName: "FluentUI-TeachingBubble" */ '@fluentui/react/lib/TeachingBubble').then((module) => ({ default: module.TeachingBubbleContent })));
export const TeachingBubbleContentBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-TeachingBubble" */ '@fluentui/react/lib/TeachingBubble').then((module) => ({ default: module.TeachingBubbleContentBase })));

