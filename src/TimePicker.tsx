import React from 'react';
export const TimePicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-TimePicker" */ '@fluentui/react/lib/TimePicker').then((module) => ({ default: module.TimePicker })));

