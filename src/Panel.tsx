import React from 'react';
export const Panel = React.lazy(() => import(/* webpackChunkName: "FluentUI-Panel" */ '@fluentui/react/lib/Panel').then((module) => ({ default: module.Panel })));
export const PanelBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Panel" */ '@fluentui/react/lib/Panel').then((module) => ({ default: module.PanelBase })));
export { PanelType } from '@fluentui/react/lib/Panel';
