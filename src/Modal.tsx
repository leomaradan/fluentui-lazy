import React from 'react';
export const Modal = React.lazy(() => import(/* webpackChunkName: "FluentUI-Modal" */ '@fluentui/react/lib/Modal').then((module) => ({ default: module.Modal })));
export const ModalBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Modal" */ '@fluentui/react/lib/Modal').then((module) => ({ default: module.ModalBase })));

