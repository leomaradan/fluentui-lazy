import React from 'react';
export const Link = React.lazy(() => import(/* webpackChunkName: "FluentUI-Link" */ '@fluentui/react/lib/Link').then((module) => ({ default: module.Link })));
export const LinkBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Link" */ '@fluentui/react/lib/Link').then((module) => ({ default: module.LinkBase })));

