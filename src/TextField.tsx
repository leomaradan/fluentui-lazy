import React from 'react';
export const TextField = React.lazy(() => import(/* webpackChunkName: "FluentUI-TextField" */ '@fluentui/react/lib/TextField').then((module) => ({ default: module.TextField })));
export const TextFieldBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-TextField" */ '@fluentui/react/lib/TextField').then((module) => ({ default: module.TextFieldBase })));
export const MaskedTextField = React.lazy(() => import(/* webpackChunkName: "FluentUI-TextField" */ '@fluentui/react/lib/TextField').then((module) => ({ default: module.MaskedTextField })));
export { getTextFieldStyles, DEFAULT_MASK_CHAR } from '@fluentui/react/lib/TextField';
