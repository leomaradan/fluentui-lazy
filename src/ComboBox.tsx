import React from 'react';
export const ComboBox = React.lazy(() => import(/* webpackChunkName: "FluentUI-ComboBox" */ '@fluentui/react/lib/ComboBox').then((module) => ({ default: module.ComboBox })));
export { VirtualizedComboBox } from '@fluentui/react/lib/ComboBox';
