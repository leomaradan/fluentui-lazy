import React from 'react';
export const Toggle = React.lazy(() => import(/* webpackChunkName: "FluentUI-Toggle" */ '@fluentui/react/lib/Toggle').then((module) => ({ default: module.Toggle })));
export const ToggleBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Toggle" */ '@fluentui/react/lib/Toggle').then((module) => ({ default: module.ToggleBase })));

