import React from 'react';
export const Breadcrumb = React.lazy(() => import(/* webpackChunkName: "FluentUI-Breadcrumb" */ '@fluentui/react/lib/Breadcrumb').then((module) => ({ default: module.Breadcrumb })));
export const BreadcrumbBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Breadcrumb" */ '@fluentui/react/lib/Breadcrumb').then((module) => ({ default: module.BreadcrumbBase })));

