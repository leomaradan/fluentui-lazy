import React from 'react';
export const Announced = React.lazy(() => import(/* webpackChunkName: "FluentUI-Announced" */ '@fluentui/react/lib/Announced').then((module) => ({ default: module.Announced })));
export const AnnouncedBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Announced" */ '@fluentui/react/lib/Announced').then((module) => ({ default: module.AnnouncedBase })));

