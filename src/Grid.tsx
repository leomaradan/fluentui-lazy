import React from 'react';
export const ButtonGrid = React.lazy(() => import(/* webpackChunkName: "FluentUI-Grid" */ '@fluentui/react/lib/Grid').then((module) => ({ default: module.ButtonGrid })));
export { ButtonGridCell } from '@fluentui/react/lib/Grid';
