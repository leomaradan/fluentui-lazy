import React from 'react';
export const Image = React.lazy(() => import(/* webpackChunkName: "FluentUI-Image" */ '@fluentui/react/lib/Image').then((module) => ({ default: module.Image })));
export const ImageBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Image" */ '@fluentui/react/lib/Image').then((module) => ({ default: module.ImageBase })));
export { ImageFit, ImageCoverStyle, ImageLoadState } from '@fluentui/react/lib/Image';
