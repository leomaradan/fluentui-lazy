import React from 'react';
export const StackItem = React.lazy(() => import(/* webpackChunkName: "FluentUI-Stack" */ '@fluentui/react/lib/Stack').then((module) => ({ default: module.StackItem })));
export const Stack = React.lazy(() => import(/* webpackChunkName: "FluentUI-Stack" */ '@fluentui/react/lib/Stack').then((module) => ({ default: module.Stack })));

