import React from 'react';
export const VerticalDivider = React.lazy(() => import(/* webpackChunkName: "FluentUI-Divider" */ '@fluentui/react/lib/Divider').then((module) => ({ default: module.VerticalDivider })));

