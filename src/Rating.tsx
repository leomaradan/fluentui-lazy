import React from 'react';
export const Rating = React.lazy(() => import(/* webpackChunkName: "FluentUI-Rating" */ '@fluentui/react/lib/Rating').then((module) => ({ default: module.Rating })));
export const RatingBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Rating" */ '@fluentui/react/lib/Rating').then((module) => ({ default: module.RatingBase })));
export { RatingSize } from '@fluentui/react/lib/Rating';
