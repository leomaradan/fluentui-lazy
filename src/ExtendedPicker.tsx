import React from 'react';
export const BaseExtendedPicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-ExtendedPicker" */ '@fluentui/react/lib/ExtendedPicker').then((module) => ({ default: module.BaseExtendedPicker })));
export const BaseExtendedPeoplePicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-ExtendedPicker" */ '@fluentui/react/lib/ExtendedPicker').then((module) => ({ default: module.BaseExtendedPeoplePicker })));
export const ExtendedPeoplePicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-ExtendedPicker" */ '@fluentui/react/lib/ExtendedPicker').then((module) => ({ default: module.ExtendedPeoplePicker })));

