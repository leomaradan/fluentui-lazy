import React from 'react';
export const ColorPicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-ColorPicker" */ '@fluentui/react/lib/ColorPicker').then((module) => ({ default: module.ColorPicker })));
export const ColorPickerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-ColorPicker" */ '@fluentui/react/lib/ColorPicker').then((module) => ({ default: module.ColorPickerBase })));

