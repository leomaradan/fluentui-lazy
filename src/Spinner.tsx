import React from 'react';
export const Spinner = React.lazy(() => import(/* webpackChunkName: "FluentUI-Spinner" */ '@fluentui/react/lib/Spinner').then((module) => ({ default: module.Spinner })));
export const SpinnerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Spinner" */ '@fluentui/react/lib/Spinner').then((module) => ({ default: module.SpinnerBase })));
export { SpinnerSize, SpinnerType } from '@fluentui/react/lib/Spinner';
