import React from 'react';
export const GroupedListV2_unstable = React.lazy(() => import(/* webpackChunkName: "FluentUI-GroupedListV2" */ '@fluentui/react/lib/GroupedListV2').then((module) => ({ default: module.GroupedListV2_unstable })));

