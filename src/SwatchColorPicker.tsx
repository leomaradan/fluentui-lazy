import React from 'react';
export const SwatchColorPicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-SwatchColorPicker" */ '@fluentui/react/lib/SwatchColorPicker').then((module) => ({ default: module.SwatchColorPicker })));
export const SwatchColorPickerBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-SwatchColorPicker" */ '@fluentui/react/lib/SwatchColorPicker').then((module) => ({ default: module.SwatchColorPickerBase })));
export const ColorPickerGridCell = React.lazy(() => import(/* webpackChunkName: "FluentUI-SwatchColorPicker" */ '@fluentui/react/lib/SwatchColorPicker').then((module) => ({ default: module.ColorPickerGridCell })));
export { ColorPickerGridCellBase } from '@fluentui/react/lib/SwatchColorPicker';
