import React from 'react';
export const SeparatorBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-Separator" */ '@fluentui/react/lib/Separator').then((module) => ({ default: module.SeparatorBase })));
export const Separator = React.lazy(() => import(/* webpackChunkName: "FluentUI-Separator" */ '@fluentui/react/lib/Separator').then((module) => ({ default: module.Separator })));

