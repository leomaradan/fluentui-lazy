import React from 'react';
export const ProgressIndicator = React.lazy(() => import(/* webpackChunkName: "FluentUI-ProgressIndicator" */ '@fluentui/react/lib/ProgressIndicator').then((module) => ({ default: module.ProgressIndicator })));
export const ProgressIndicatorBase = React.lazy(() => import(/* webpackChunkName: "FluentUI-ProgressIndicator" */ '@fluentui/react/lib/ProgressIndicator').then((module) => ({ default: module.ProgressIndicatorBase })));

