import React from 'react';
export const WeeklyDayPicker = React.lazy(() => import(/* webpackChunkName: "FluentUI-WeeklyDayPicker" */ '@fluentui/react/lib/WeeklyDayPicker').then((module) => ({ default: module.WeeklyDayPicker })));
export { defaultWeeklyDayPickerStrings, defaultWeeklyDayPickerNavigationIcons } from '@fluentui/react/lib/WeeklyDayPicker';
